<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'unit', 'category'
    ];

    public function ingredients()
    {
        return $this->belongsToMany('App\RecipeIngredient')
            ->withTimestamps();
    }
}
