<?php

namespace App\Http\Controllers;

use App\Recipe;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $recipes = Recipe::latest()->paginate(1);

        return view('landing', compact('recipes'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function recipe($id)
    {
        $recipe = Recipe::find($id);

        return view('recipe', compact('recipe'));
    }

    /**
     * Show the search results.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $recipes = Recipe::search($request->searchEntry)->paginate(15);

        return view('search', compact('recipes'));
    }
}
