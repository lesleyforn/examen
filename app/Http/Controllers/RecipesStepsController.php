<?php

namespace App\Http\Controllers;

use App\Recipe;
use App\RecipeStep;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class RecipesStepsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Recipe $recipe
     * @param RecipeStep $step
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Recipe $recipe, RecipeStep $step)
    {
        return view('user.recipes.edit', compact('recipe', 'step'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Recipe $recipe
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Recipe $recipe, Request $request)
    {
        $request->validate([
            'step_number' => 'required|integer|'.Rule::unique('recipe_steps')->where('recipe_id', $recipe->id),
            'instructions' => 'required'
        ]);


        $step = new Recipestep();

        if(isset($request->step_number)){ $step->step_number = $request->step_number; }
        if(isset($request->instructions)){ $step->instructions = $request->instructions; }

        $recipe->recipeSteps()->save($step);

        return redirect()->back()->with('success', 'Step successfully added');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Recipe $recipe
     * @param RecipeStep $step
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Recipe $recipe, RecipeStep $step, Request $request)
    {

        if($request->step_number == $step->step_number){            
            request()->validate([
                'step_number' => 'required|integer',
                'instructions' => 'required'
            ]);
        }else{
            request()->validate([
                'step_number' => 'required|integer|'. Rule::unique('recipe_steps')->where('recipe_id', $recipe->id),
                'instructions' => 'required'
            ]);
        }

        if($step->step_number !== $request->step_number){ $step->step_number = $request->step_number; }
        if($step->instructions !== $request->instructions){ $step->instructions = $request->instructions; }

        $step->save();

        return redirect()->route('recipes.edit', $recipe->id)->with('success', 'Step updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Recipe $recipe
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Recipe $recipe, $id)
    {
        $recipe->recipeSteps()->find($id)->delete();
        return redirect()->back()->with('success', 'Step deleted successfully');
    }
}
