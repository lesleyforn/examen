<?php

namespace App\Http\Controllers;

use App\Ingredient;
use App\Recipe;
use App\Unit;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class RecipesController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $recipes = Auth::user()->admin != true ? Auth::user()->recipes->paginate(5) : Recipe::latest()->paginate(5);
        return view('user.recipes.index', compact('recipes'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'title' => 'required|string|max:255',
            'desc' => 'required'
        ]);

        $recipe = new Recipe;

        if(isset($request->title)){ $recipe->title = strtolower($request->title); }
        if(isset($request->desc)){ $recipe->desc = strtolower($request->desc); }
        if($request->hasFile('image')){
            $recipe->image = $request->image->store('public');
        }

        $recipe->save();

        Auth::user()->recipes()->attach($recipe->id);

        return redirect()->route('recipes.show', $recipe->id)
            ->with('success','Recipe created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function show(Recipe $recipe)
    {
        $ingredientsList = Ingredient::all();
        $unitsList = Unit::all();

        return view('user.recipes.show', compact('recipe', 'ingredientsList', 'unitsList'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function edit(Recipe $recipe)
    {
        $ingredientsList = Ingredient::all();
        $unitsList = Unit::all();

        return view('user.recipes.show', compact('recipe','ingredientsList', 'unitsList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Recipe $recipe)
    {
        request()->validate([
            'title' => 'required|string|max:255',
            'desc' => 'required',
        ]);

        if($recipe->title !== strtolower($request->title)){
            $recipe->title = strtolower($request->title);
        }

        if($recipe->desc !== strtolower($request->desc)){
            $recipe->desc = strtolower($request->desc);
        }

        if($request->hasFile('image')){
            Storage::delete($recipe->image);
            $recipe->image = $request->image->store('public');
        }

        $recipe->update();

        return redirect()->route('recipes.index')
            ->with('success','Recipe updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Recipe $recipe
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Recipe $recipe)
    {

        if($recipe->ingredients !== null){
            foreach($recipe->ingredients as $ingredient)
            {
                $recipe->ingredients()->detach($ingredient->id);
            }
        }

        if($recipe->steps !== null){
            foreach($recipe->steps as $step)
            {
                $recipe->steps()->delete($step->id);
            }
        }

        if($recipe->image !== null){
            Storage::delete($recipe->image);
        }


        $user = User::find(Auth::user()->id);

        if($user !== null){
            $user->recipes()->detach($recipe->id);
        }

        $recipe->delete();

        return redirect()->route('recipes.index')
            ->with('success','Recipe and image deleted successfully');
    }
}
