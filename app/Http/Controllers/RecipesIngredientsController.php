<?php

namespace App\Http\Controllers;

use App\Unit;
use App\Recipe;
use App\Ingredient;
use App\RecipeIngredient;
use Illuminate\Http\Request;

class RecipesIngredientsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Recipe $recipe
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Recipe $recipe, Request $request)
    {
        $request->validate([
            'quantity' => 'required|integer',
            'measurements' => 'required',
            'ingredient' => 'required'
        ]);

        $unit = Unit::find($request->measurements);
        $ingredient = Ingredient::where('name', $request->ingredient)->first();

        if($ingredient == null){
            $ingredient = new Ingredient;
            
            $ingredient->name = $request->ingredient;
            $ingredient->save();

        }

        $recipeIngredients = new RecipeIngredient;
        $recipeIngredients->amount = $request->quantity;

        $recipe->recipeIngredients()->save($recipeIngredients);
        $recipeIngredients->units()->attach($unit->id);
        $recipeIngredients->ingredients()->attach($ingredient->id);

        return redirect()->back()->with('success', 'Step successfully added');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Recipe $recipe
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Recipe $recipe, $id)
    {
        $ingredientsList = Ingredient::all();
        $unitsList = Unit::all();
        $recipeIngredient = $recipe->recipeIngredients()->find($id);

        return view('user.recipes.edit', compact('recipe', 'recipeIngredient', 'ingredientsList', 'unitsList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param
     * @return \Illuminate\Http\Response
     */
    public function update(Recipe $recipe, $id, Request $request)
    {
        $request->validate([
            'quantity' => 'required|integer',
            'measurements' => 'required',
            'ingredient' => 'required'
        ]);

        $unit = Unit::find($request->measurements);
        $ingredient = Ingredient::where('name', $request->ingredient)->first();

        $recipeIngredient = $recipe->recipeIngredients()->find($id);

        if($recipeIngredient->amount !== $request->quantity){
            $recipeIngredient->amount = $request->quantity;
        }

        $recipeIngredient->save();

        $recipeIngredient->units()->detach($recipeIngredient->units->first()->id);
        $recipeIngredient->units()->attach($unit);

        $recipeIngredient->ingredients()->detach($recipeIngredient->ingredients->first()->id);
        $recipeIngredient->ingredients()->attach($ingredient);


        return redirect()->route('recipes.edit', $recipe->id)->with('success', 'Ingredient updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Recipe $recipe
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Recipe $recipe, $id)
    {
        $recipeIngredient = $recipe->recipeIngredients()->find($id);

        if($recipeIngredient->units !== null){
            foreach($recipeIngredient->units as $unit)
            {
                $recipeIngredient->units()->detach($unit->id);
            }
        }

        if($recipeIngredient->ingredients !== null){
            foreach($recipeIngredient->ingredients as $ingredient)
            {
                $recipeIngredient->ingredients()->detach($ingredient->id);
            }
        }

        $recipe->recipeIngredients()->find($id)->delete();

        return redirect()->back()->with('success', 'Ingredient deleted successfully');
    }
}
