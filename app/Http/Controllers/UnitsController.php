<?php

namespace App\Http\Controllers;

use App\Unit;
use Illuminate\Http\Request;

class UnitsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $units = Unit::latest()->paginate(6);

        return view('user.units.index', compact('units'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function edit(Unit $unit)
    {
        $units = Unit::latest()->paginate(6);

        return view('user.units.index', compact('unit', 'units'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required|string|max:255',
            'unit' => 'required|string|max:255]unique:units,unit',
            'group' => 'required'
        ]);

        $unit = new Unit;
        if(isset($request->name)){ $unit->name = strtolower($request->name);}
        if(isset($request->unit)){ $unit->unit = strtolower($request->unit);}
        if(isset($request->group)){ $unit->group = strtolower($request->group);}
        $unit->save();

        return redirect()->back()->with('success', 'Unit successfully created');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request, Unit $unit)
    {
        request()->validate([
            'name' => 'required|string|max:255',
            'unit' => 'required|string|max:255]unique:units,unit',
            'group' => 'required'
        ]);

        if($unit->name !== strtolower($request->name)){
            $unit->name = strtolower($request->name);
        }
        if($unit->unit !== strtolower($request->unit)){
            $unit->unit = strtolower($request->unit);
        }
        if($unit->group !== strtolower($request->group)){
            $unit->group = strtolower($request->group);
        }
        $unit->update();

        return redirect()->route('units.index')->with('success', 'Unit updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Unit $unit
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Unit $unit)
    {
        $unit->delete();
        return redirect()->back()->with('success', 'Unit deleted successfully');
    }
}
