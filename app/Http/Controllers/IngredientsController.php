<?php

namespace App\Http\Controllers;

use App\Ingredient;
use Illuminate\Http\Request;

class IngredientsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $ingredients = Ingredient::latest()->paginate(8);

        return view('user.ingredients.index', compact('ingredients'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ingredient  $ingredient
     * @return \Illuminate\Http\Response
     */
    public function edit(Ingredient $ingredient)
    {
        $ingredients = Ingredient::latest()->orderBy('name')->paginate(10);
        return view('user.ingredients.index', compact('ingredient', 'ingredients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required|string|max:255'
        ]);

            $newIngredient = new Ingredient;

            if(isset($request->name)){
                $newIngredient->name = strtolower($request->name);
                $newIngredient->save();
            }

        return redirect()->back()->with('success', 'Ingredient was created successfully');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ingredient  $ingredient
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(Request $request, Ingredient $ingredient)
    {
        request()->validate([
            'name' => 'required|string|max:255'
        ]);

        if(isset($request->name) && $ingredient->name !== strtolower($request->name)){
            $ingredient->name = strtolower($request->name);
            $ingredient->update();
        }

        return redirect()->route('units.index')->with('success', 'Ingredient updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ingredient $ingredient
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Ingredient $ingredient)
    {

        $ingredient->delete();
        return redirect()->back()->with('success', 'Ingredient deleted successfully');
    }
}
