<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecipeIngredient extends Model
{
    protected $fillable = [
        'amount'
    ];

    public function recipes()
    {
        return $this->belongsTo('App\Recipe');
    }

    public function ingredients()
    {
        return $this->belongsToMany('App\Ingredient')
            ->withTimestamps();
    }

    public function units()
    {
        return $this->belongsToMany('App\Unit')
            ->withTimestamps();
    }
}
