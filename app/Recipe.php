<?php

namespace App;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    use Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'desc', 'image'
    ];

    public function users()
    {
        return $this
            ->belongsToMany('App\User')
            ->withTimestamps();
    }

    public function recipeSteps()
    {
        return $this
            ->hasMany('App\recipeStep');
    }

    public function recipeIngredients()
    {
        return $this
            ->hasMany('App\recipeIngredient');
    }

    public function hasIngredients($tagToMatch)
    {
        foreach ($this->recipeIngredients->ingredients as $ingredient)
        {
            if ($ingredient->id == $tagToMatch)
                return (true);
        }
        return (false);
    }
}
