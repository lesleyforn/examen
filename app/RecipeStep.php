<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecipeStep extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'step_nr', 'desc'
    ];

    public function recipes()
    {
        return $this->belongsTo('App\Recipe');
    }
}
