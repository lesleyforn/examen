<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User;

        $admin->name = 'Administrator';
        $admin->email = 'admin@site.be';
        $admin->password = bcrypt('password');
        $admin->admin = 1;

        $admin->save();

        $user = new User;

        $user->name = 'Dummy User';
        $user->email = 'user@site.be';
        $user->password = bcrypt('password');
        $user->admin = 0;

        $user->save();
    }
}