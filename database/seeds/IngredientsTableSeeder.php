<?php

use Illuminate\Database\Seeder;
use App\Ingredient;

class IngredientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ingredient = new Ingredient;
        $ingredient->name = 'zalm (zonder vel)';
        $ingredient->save();
        unset($ingredient);

        $ingredient = new Ingredient;
        $ingredient->name = 'sesamzaadje (zwart)';
        $ingredient->save();
        unset($ingredient);

        $ingredient = new Ingredient;
        $ingredient->name = 'sojasaus';
        $ingredient->save();
        unset($ingredient);

        $ingredient = new Ingredient;
        $ingredient->name = 'hoisinsaus';
        $ingredient->save();
        unset($ingredient);

        $ingredient = new Ingredient;
        $ingredient->name = 'kippengehakt';
        $ingredient->save();
        unset($ingredient);

        $ingredient = new Ingredient;
        $ingredient->name = 'ijsbergsla';
        $ingredient->save();
        unset($ingredient);

        $ingredient = new Ingredient;
        $ingredient->name = 'sjalot';
        $ingredient->save();
        unset($ingredient);

        $ingredient = new Ingredient;
        $ingredient->name = 'munt (gesnipperd)';
        $ingredient->save();
        unset($ingredient);

        $ingredient = new Ingredient;
        $ingredient->name = 'rijstnoedels';
        $ingredient->save();
        unset($ingredient);

        $ingredient = new Ingredient;
        $ingredient->name = 'kippenbouillon';
        $ingredient->save();
        unset($ingredient);

        $ingredient = new Ingredient;
        $ingredient->name = 'suiker';
        $ingredient->save();
        unset($ingredient);

        $ingredient = new Ingredient;
        $ingredient->name = 'limoensap';
        $ingredient->save();
        unset($ingredient);

        $ingredient = new Ingredient;
        $ingredient->name = 'rode chilipeper (gesnipperd)';
        $ingredient->save();
        unset($ingredient);

        $ingredient = new Ingredient;
        $ingredient->name = 'lente-uien';
        $ingredient->save();
        unset($ingredient);

        $ingredient = new Ingredient;
        $ingredient->name = 'koriander (gesnipperd)';
        $ingredient->save();
        unset($ingredient);

        $ingredient = new Ingredient;
        $ingredient->name = 'cashewnoten';
        $ingredient->save();
        unset($ingredient);

        $ingredient = new Ingredient;
        $ingredient->name = 'vissaus';
        $ingredient->save();
        unset($ingredient);

        $ingredient = new Ingredient;
        $ingredient->name = 'chilipoeder';
        $ingredient->save();
        unset($ingredient);

    }
}
