<?php

use Illuminate\Database\Seeder;

class UnitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('units')->insert([
            /*[ 'name' => '' , 'unit' => '' , 'group' => 'gewicht' ]
            [ 'name' => '' , 'unit' => '' , 'group' => 'inhoud' ]
            [ 'name' => '' , 'unit' => '' , 'group' => 'voorwerp' ]*/
            [ 'name' => 'pound' , 'unit' => 'lb' , 'group' => 'gewicht' ],
            [ 'name' => 'gram' , 'unit' => 'gr' , 'group' => 'gewicht' ],
            [ 'name' => 'ounce' , 'unit' => 'oz' , 'group' => 'gewicht' ],
            [ 'name' => 'liter' , 'unit' => 'L' , 'group' => 'inhoud' ],
            [ 'name' => 'deciliter ' , 'unit' => 'dl' , 'group' => 'inhoud' ],
            [ 'name' => 'centiliter ' , 'unit' => 'cl' , 'group' => 'inhoud' ],
            [ 'name' => 'milliliter' , 'unit' => 'ml / cc' , 'group' => 'inhoud' ],
            [ 'name' => 'cup' , 'unit' => 'lb' , 'C' => 'inhoud' ],
            [ 'name' => 'pint' , 'unit' => 'lb' , 'Pt' => 'inhoud' ],
            [ 'name' => 'fluid ounce' , 'unit' => 'fl. oz' , 'group' => 'inhoud' ],
            [ 'name' => 'eetlepel' , 'unit' => 'el' , 'group' => 'voorwerp' ],
            [ 'name' => 'theelepel' , 'unit' => 'tl' , 'group' => 'voorwerp' ],
        ]);
    }
}
