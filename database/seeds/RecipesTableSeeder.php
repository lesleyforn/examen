<?php

use App\Recipe;
use App\RecipeIngredient;
use App\RecipeStep;
use App\User;
use Illuminate\Database\Seeder;

class RecipesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::find(1);

        /*Recipe 1*/
        $recipe1 = new Recipe;
        $recipe1->title= 'oosterse zalm op cederhout';
        $recipe1->desc= 'barbecue expert gilles draps deelt speciaal voor project libelle zijn beste recept voor zalm op de bbq.';
        $recipe1->image= 'public/temp-1.jpg';
        $recipe1->save();
        $admin->recipes()->attach($recipe1->id);

        /*Ingredients*/
        $recipeIngredients = new RecipeIngredient;
        $recipeIngredients->recipe_id = 1;
        $recipeIngredients->amount = 250;
        $recipe1->recipeIngredients()->save($recipeIngredients);
        $recipeIngredients->ingredients()->attach(1);
        $recipeIngredients->units()->attach(2);
        unset($recipeIngredients);

        $recipeIngredients = new RecipeIngredient;
        $recipeIngredients->recipe_id = 1;
        $recipeIngredients->amount = 10;
        $recipe1->recipeIngredients()->save($recipeIngredients);
        $recipeIngredients->ingredients()->attach(2);
        $recipeIngredients->units()->attach(2);
        unset($recipeIngredients);

        $recipeIngredients = new RecipeIngredient;
        $recipeIngredients->recipe_id = 1;
        $recipeIngredients->amount = 1;
        $recipe1->recipeIngredients()->save($recipeIngredients);
        $recipeIngredients->ingredients()->attach(3);
        $recipeIngredients->units()->attach(11);
        unset($recipeIngredients);

        $recipeIngredients = new RecipeIngredient;
        $recipeIngredients->recipe_id = 1;
        $recipeIngredients->amount = 1;
        $recipe1->recipeIngredients()->save($recipeIngredients);
        $recipeIngredients->ingredients()->attach(4);
        $recipeIngredients->units()->attach(11);
        unset($recipeIngredients);


        /*Steps*/
        $recipeSteps = new RecipeStep;
        $recipeSteps->step_number = 1;
        $recipeSteps->instructions = 'Leg de rookplank minstens een uur onder water. zet een gewicht op de plank zodat ze ondergedompeld blijft.';
        $recipe1->recipeSteps()->save($recipeSteps);
        unset($recipeSteps);

        $recipeSteps = new RecipeStep;
        $recipeSteps->step_number = 2;
        $recipeSteps->instructions = 'Verwarm de barbecue tot 200°C.';
        $recipe1->recipeSteps()->save($recipeSteps);
        unset($recipeSteps);

        $recipeSteps = new RecipeStep;
        $recipeSteps->step_number = 3;
        $recipeSteps->instructions = 'Snij de zalm in blokjes van 2 op 2 cm.';
        $recipe1->recipeSteps()->save($recipeSteps);
        unset($recipeSteps);

        $recipeSteps = new RecipeStep;
        $recipeSteps->step_number = 4;
        $recipeSteps->instructions = 'Droog het rookplankje goed af en leg de zalm erop. Plaats op de barbecue en sluit af met het deksel. Laat 5 minuten garen.';
        $recipe1->recipeSteps()->save($recipeSteps);
        unset($recipeSteps);

        $recipeSteps = new RecipeStep;
        $recipeSteps->step_number = 5;
        $recipeSteps->instructions = 'Werk af met de sojasaus, de hoisinsaus en de zwarte sesamzaadjes.';
        $recipe1->recipeSteps()->save($recipeSteps);
        unset($recipeSteps);



        /*Recipe 2*/
        $recipe2 = new Recipe;
        $recipe2->title= 'Laab Khai';
        $recipe2->desc= 'een lekker gerecht met kip dat je in 30 minuten maakt.';
        $recipe2->image= 'public/temp-2.jpg';

        $recipe2->save();
        $admin->recipes()->attach($recipe2->id);

        /* Ingredients */
        $recipeIngredients = new RecipeIngredient;
        $recipeIngredients->recipe_id = 2;
        $recipeIngredients->amount = 400;
        $recipe2->recipeIngredients()->save($recipeIngredients);
        $recipeIngredients->ingredients()->attach(5);
        $recipeIngredients->units()->attach(2);
        unset($recipeIngredients);

        $recipeIngredients = new RecipeIngredient;
        $recipeIngredients->recipe_id = 2;
        $recipeIngredients->amount = 40;
        $recipe2->recipeIngredients()->save($recipeIngredients);
        $recipeIngredients->ingredients()->attach(12);
        $recipeIngredients->units()->attach(7);
        unset($recipeIngredients);

        $recipeIngredients = new RecipeIngredient;
        $recipeIngredients->recipe_id = 2;
        $recipeIngredients->amount = 400;
        $recipe2->recipeIngredients()->save($recipeIngredients);
        $recipeIngredients->ingredients()->attach(6);
        $recipeIngredients->units()->attach(2);
        unset($recipeIngredients);

        $recipeIngredients = new RecipeIngredient;
        $recipeIngredients->recipe_id = 2;
        $recipeIngredients->amount = 10;
        $recipe2->recipeIngredients()->save($recipeIngredients);
        $recipeIngredients->ingredients()->attach(13);
        $recipeIngredients->units()->attach(1);
        unset($recipeIngredients);


        $recipeIngredients = new RecipeIngredient;
        $recipeIngredients->recipe_id = 2;
        $recipeIngredients->amount = 5;
        $recipe2->recipeIngredients()->save($recipeIngredients);
        $recipeIngredients->ingredients()->attach(7);
        $recipeIngredients->units()->attach(2);
        unset($recipeIngredients);

        $recipeIngredients = new RecipeIngredient;
        $recipeIngredients->recipe_id = 2;
        $recipeIngredients->amount = 3;
        $recipe2->recipeIngredients()->save($recipeIngredients);
        $recipeIngredients->ingredients()->attach(14);
        $recipeIngredients->units()->attach(2);
        unset($recipeIngredients);

        $recipeIngredients = new RecipeIngredient;
        $recipeIngredients->recipe_id = 2;
        $recipeIngredients->amount = 1;
        $recipe2->recipeIngredients()->save($recipeIngredients);
        $recipeIngredients->ingredients()->attach(8);
        $recipeIngredients->units()->attach(2);
        unset($recipeIngredients);

        $recipeIngredients = new RecipeIngredient;
        $recipeIngredients->recipe_id = 2;
        $recipeIngredients->amount = 1;
        $recipe2->recipeIngredients()->save($recipeIngredients);
        $recipeIngredients->ingredients()->attach(15);
        $recipeIngredients->units()->attach(2);
        unset($recipeIngredients);

        $recipeIngredients = new RecipeIngredient;
        $recipeIngredients->recipe_id = 2;
        $recipeIngredients->amount = 200;
        $recipe2->recipeIngredients()->save($recipeIngredients);
        $recipeIngredients->ingredients()->attach(9);
        $recipeIngredients->units()->attach(2);
        unset($recipeIngredients);

        $recipeIngredients = new RecipeIngredient;
        $recipeIngredients->recipe_id = 2;
        $recipeIngredients->amount = 2;
        $recipe2->recipeIngredients()->save($recipeIngredients);
        $recipeIngredients->ingredients()->attach(16);
        $recipeIngredients->units()->attach(11);
        unset($recipeIngredients);

        $recipeIngredients = new RecipeIngredient;
        $recipeIngredients->recipe_id = 2;
        $recipeIngredients->amount = 10;
        $recipe2->recipeIngredients()->save($recipeIngredients);
        $recipeIngredients->ingredients()->attach(10);
        $recipeIngredients->units()->attach(2);
        unset($recipeIngredients);

        $recipeIngredients = new RecipeIngredient;
        $recipeIngredients->recipe_id = 2;
        $recipeIngredients->amount = 2;
        $recipe2->recipeIngredients()->save($recipeIngredients);
        $recipeIngredients->ingredients()->attach(17);
        $recipeIngredients->units()->attach(11);
        unset($recipeIngredients);

        $recipeIngredients = new RecipeIngredient;
        $recipeIngredients->recipe_id = 2;
        $recipeIngredients->amount = 1000;
        $recipe2->recipeIngredients()->save($recipeIngredients);
        $recipeIngredients->ingredients()->attach(11);
        $recipeIngredients->units()->attach(2);
        unset($recipeIngredients);

        $recipeIngredients = new RecipeIngredient;
        $recipeIngredients->recipe_id = 2;
        $recipeIngredients->amount = 1;
        $recipe2->recipeIngredients()->save($recipeIngredients);
        $recipeIngredients->ingredients()->attach(18);
        $recipeIngredients->units()->attach(12);
        unset($recipeIngredients);

        /*Steps*/
        $recipeSteps = new RecipeStep;
        $recipeSteps->step_number = 1;
        $recipeSteps->instructions = ' Meng het kipgehakt met 2 eetlepels limoensap. Laat 200 ml water koken met het kippenbouillonblokje. Voeg het kippengehakt toe en roer regelmatig tot het gehakt gaar is en het water verdampt is.';
        $recipe2->recipeSteps()->save($recipeSteps);
        unset($recipeSteps);

        $recipeSteps = new RecipeStep;
        $recipeSteps->step_number = 2;
        $recipeSteps->instructions = ' Zet de pan van het vuur en voeg vissaus, 1 eetlepel limoensap, suiker, het chilipoeder en het pepertje toe. Snij de sjalot en lente-ui in fijne reepjes en voeg toe aan het gehakt. Voeg tenslotte ook de muntblaadjes en verse koriander toe.';
        $recipe2->recipeSteps()->save($recipeSteps);
        unset($recipeSteps);

        $recipeSteps = new RecipeStep;
        $recipeSteps->step_number = 3;
        $recipeSteps->instructions = ' Bereid de noedels volgens de instructies op de verpakking, giet af. Maak enkele mooie blaadjes ijsbergsla los van de krop en leg ze in een kommetje.';
        $recipe2->recipeSteps()->save($recipeSteps);
        unset($recipeSteps);

        $recipeSteps = new RecipeStep;
        $recipeSteps->step_number = 4;
        $recipeSteps->instructions = 'Dresseer er het gehakt en de noedels in. Garneer met koriander en gehakte cashewnoten.';
        $recipe2->recipeSteps()->save($recipeSteps);
        unset($recipeSteps);
    }
}