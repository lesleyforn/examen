@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                @include('forms.search')
            </div>
        </div>

        <div class="row justify-content-center">
            <header class="col-sm-12">
                <h1 class="h2">Search results:</h1>
            </header>
        </div>

        <div class="row d-flex">

        @if($recipes->count() !== 0)
            @foreach($recipes as $recipe)
                <div class="col-sm-12 col-md-3">
                    <div class="card">
                        @if(Storage::disk('local')->url($recipe->image) !== null)
                            <img class="card-img-top" src="{{ Storage::disk('local')->url($recipe->image) }}" alt="">
                        @endif
                        <div class="card-body">
                            <h4 class="card-title">{{ $recipe->title }}</h4>
                            <p class="card-text">{{ $recipe->desc }}</p>
                            <label>Ingredienten:</label>
                            @if(isset($recipe->recipeIngredients))
                            <ul>
                                {{--@foreach($recipeIngredient->ingredients as $ingredient)
                                    {{$ingredient->name}}
                                @endforeach--}}
                                @foreach($recipe->recipeIngredients as $ingredient)

                                    <li>{{ $ingredient->ingredients->first()->name}}</li>

                                @endforeach
                            </ul>
                            @endif
                            <div class="d-flex justify-content-between btn-group-sm bottom">
                            <a href="{{ route('public.recipe', $recipe->id) }}" class="btn btn-sm btn-primary">Read more</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Sorry, No results found...</h5>
                    </div>
                </div>
            </div>
        @endif
        </div>
        <div class="row">
            <div class="col-sm-12 mt-4 text-right">
                {{$recipes->links()}}
            </div>
        </div>
    </div>
@endsection
