@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                @include('forms.search')
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12 col-md-4">
                                @if(Storage::disk('local')->url($recipe->image) !== null)
                                    <img class="img-fluid" src="{{ Storage::disk('local')->url($recipe->image) }}" alt="">
                                @endif
                            </div>
                            <div class="col-sm-12 col-md-8">
                                <h1 class="card-title h2">Recipe: {{ $recipe->title }}</h1>
                                <p class="card-text"> {{ $recipe->desc }}</p>
                                <br>
                                <br>
                                <p class="card-text"> Ingredients:</p>
                                <ul class="duel-list">
                                    @foreach($recipe->recipeIngredients as $recipeIngredients)
                                        @foreach($recipeIngredients->ingredients as $ingredient)
                                        <li>{{$ingredient->name}}</li>
                                        @endforeach
                                    @endforeach
                                </ul>
                            </div>
                            <div class="col-sm-12 mt-4">
                                <p class="card-text"> Bereidingswijze:</p>
                                <ol>
                                    @foreach($recipe->recipeSteps->sortBy('step_number') as $stap)
                                        <li>{{ $stap->instructions }}</li>
                                    @endforeach
                                </ol>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
