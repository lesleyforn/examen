<div class="form-group row">
    <label for="ingredientDesc" class="col-sm-4 col-md-3 col-lg-2 col-form-label">Unit:</label>
    <div class="col-sm-8 col-md-9 col-lg-10">
        <input name="unit" id="ingredientDesc" class="form-control" value="{{ old('unit') !== null ? old('unit') : (isset($unit->unit) ? $unit->unit : '') }}" required>
    </div>
</div>

<div class="form-group row">
    <label for="ingredientTitle" class="col-sm-4 col-md-3 col-lg-2 col-form-label">Name:</label>
    <div class="col-sm-8 col-md-9 col-lg-10">
        <input type="text" name="name" class="form-control" id="ingredientTitle" value="{{ old('name') !== null ? old('name') :  (isset($unit->name) ? $unit->name : '') }}" required>
    </div>
</div>

<div class="form-group row">
    <label for="ingredientImage" class="col-sm-4 col-md-3 col-lg-2 col-form-label">Group:</label>

    <div class="col-sm-4 col-md-9 col-lg-10">
        <select class="form-control" id="exampleFormControlSelect1" name="group">
            <option>Select one</option>
            <option value="gewicht">Gewicht</option>
            <option value="inhoud">Inhoud</option>
            <option value="voorwerp">Voorwerp</option>
        </select>
    </div>
</div>