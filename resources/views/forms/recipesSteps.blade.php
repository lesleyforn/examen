
<div class="form-group row">
    <label for="step_number" class="col-sm-4 col-md-2 col-form-label">Step number:</label>
    <div class="col-sm-8 col-md-10">
        <input type="number" id="step_number" name="step_number" class="form-control" value="{{ old('step_number') !== null ? old('step_number') : (isset($step->step_number) ? $step->step_number : '1') }}">
    </div>
</div>

<div class="form-group row">
    <label for="instructions" class="col-sm-4 col-md-2 col-form-label">description:</label>

    <div class="col-sm-8 col-md-10">
        <textarea name="instructions" id="stepDesc" class="form-control" required>{{ old('instructions') !== null ? old('instructions') : (isset($step->instructions) ? $step->instructions : "") }}</textarea>
    </div>
</div>