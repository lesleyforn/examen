<div class="form-group row">
    <label for="quantity" class="col-sm-4 col-md-3 col-lg-2 col-form-label">Quantity:</label>
    <div class="col-sm-8 col-md-3 col-lg-4">
        <input type="text" name="quantity" class="form-control" id="quantity" value="{{  old('quantity') !== null ? old('quantity') : isset($recipeIngredient) ? $recipeIngredient->amount : '' }}" required>
    </div>

    <label for="unit-dropdown" class="col-sm-4 col-md-3 col-lg-2 col-form-label">Measurements:</label>
    <div class="col-sm-8 col-md-3 col-lg-4">
        <select class="form-control" id="unit-dropdown" name="measurements" required>
            <option selected disabled hidden>Select one</option>
            @foreach($unitsList as $unit)
                <option value="{{ $unit->id }}" {{ isset($recipeIngredient) ? ($recipeIngredient->units->first()->id === $unit->id ? 'selected' : '') : '' }} >{{ $unit->unit }} / {{ $unit->name }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group row">
    <label for="ingredients-list" class="col-sm-4 col-md-3 col-lg-2 col-form-label">Ingredient:</label>
    <div class="col-sm-8 col-md-9 col-lg-10">
        <input id="ingredients-list" name="ingredient" type="text" class="form-control" list="ingredientsList" value="{{ old('quantity') !== null ? old('quantity') : isset($recipeIngredient) ? $recipeIngredient->ingredients->first()->name : '' }}" required>
        <datalist id="ingredientsList">
            @foreach($ingredientsList as $ingredient)
                <option value="{{ $ingredient->name }}"></option>
            @endforeach
        </datalist>
    </div>
</div>

