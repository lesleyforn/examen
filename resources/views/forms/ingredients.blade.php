<div class="form-group row">
    <label for="ingredientName" class="col-sm-4 col-md-3 col-lg-2 col-form-label">Ingredient Name:</label>
    <div class="col-sm-8 col-md-9 col-lg-10">
        <input type="text" name="name" class="form-control" id="ingredientName" value="{{ old('name') !== null ? old('name') :  (isset($ingredient->name) ? $ingredient->name : '') }}" required>
    </div>
</div>