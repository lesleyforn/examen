<div class="form-group row">
    <label for="ingredientTitle" class="col-sm-4 col-md-3 col-lg-2 col-form-label">Title:</label>
    <div class="col-sm-8 col-md-9 col-lg-10">
        <input type="text" name="title" class="form-control" id="ingredientTitle" value="{{ old('title') !== null ? old('title') :  (isset($recipe->title) ? $recipe->title : '') }}" required>
    </div>
</div>

<div class="form-group row">
    <label for="ingredientDesc" class="col-sm-4 col-md-3 col-lg-2 col-form-label">Description:</label>
    <div class="col-sm-8 col-md-9 col-lg-10">
        <textarea name="desc" id="ingredientDesc" class="form-control" required>{{ old('desc') !== null ? old('desc') : (isset($recipe->desc) ? $recipe->desc : '') }}</textarea>
    </div>
</div>

<div class="form-group row">
    <label for="ingredientImage" class="col-sm-4 col-md-3 col-lg-2 col-form-label">Image:</label>

    <div class="col-sm-4 col-md-5 col-lg-5">
        <input type="file" name="image" class="form-control-file" id="ingredientImage" value="{{ old('image') !== null ? old('image') :  '' }}" >
    </div>

@if(isset($recipe))
    <div class="col-sm-4 col-md-4 col-lg-5">
        @if(isset($recipe->image))
            <img src="{{Storage::disk('local')->url($recipe->image)}}" class="img-thumbnail" alt="">
        @endif
    </div>
@endif
</div>