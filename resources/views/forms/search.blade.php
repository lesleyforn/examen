<form action="{{ route('public.search') }}" method="post">
    @csrf
    <div class="input-group mb-3">
        <input name="searchEntry" type="text" placeholder="search" value="{{ old('searchEntry') }}" class="form-control">
        <div class="input-group-append">
            <button class="btn btn-outline-secondary">submit</button>
        </div>
    </div>
</form>