<div class="col-sm-12 col-md-4 col-lg-2">
    <div class="card">
        <div class="card-header">Menu</div>

        <div class="card-body">
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('recipes.index') }}">Recipes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('ingredients.index') }}">Ingredients</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('units.index') }}">Units</a>
                </li>
            </ul>
        </div>
    </div>
</div>