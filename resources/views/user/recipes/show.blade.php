@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row">

            @include('incl.user-menu')

            <div class="col-sm-12 col-md-8 col-lg-10">

                @if ($message = Session::get('success'))
                    <div class="alert alert-success mb-4">
                        <p>{{ $message }}</p>
                    </div>
                @endif

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="card mb-4">
                    <div class="card-header">Recipe Details</div>
                    <div class="card-body">
                        <form action="{{route('recipes.update', $recipe->id)}}" method="POST" enctype="multipart/form-data">
                            @method('PUT')
                            @csrf
                            @include('forms.recipes')

                            <div class="text-right">
                                <button type="submit" class="btn btn-sm btn-primary">Update</button>
                            </div>
                        </form>
                    </div>
                </div>



                <div class="card mb-4">
                    <div class="card-header">Add Ingredients</div>
                    <div class="card-body">
                        <form action="{{route('recipes.ingredient.add', $recipe->id)}}" method="POST">
                        @csrf
                        @include('forms.recipesIngredients')
                            <div class="text-right">
                                <button type="submit" class="btn btn-sm btn-primary">Add Ingredient</button>
                            </div>
                        </form>
                    </div>
                </div>


                <div class="card mb-4">
                    <div class="card-header">Recipe Ingredients</div>
                    <div class="card-body">
                        <table class="table table-sm">
                            <thead>
                            <tr>
                                <th scope="col" width="150">Quantity</th>
                                <th scope="col" width="150">Measurements</th>
                                <th scope="col">Ingredient</th>
                                <th scope="col" width="120">controls</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($recipe->recipeIngredients as $recipeIngredient)
                                <tr>
                                    <td>{{$recipeIngredient->amount}}</td>
                                    <td>
                                        @foreach($recipeIngredient->units as $unit)
                                            {{$unit->unit}} / {{$unit->name}}
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($recipeIngredient->ingredients as $ingredient)
                                            {{$ingredient->name}}
                                        @endforeach
                                    </td>
                                    <td>
                                        <form action="{{ route('recipes.ingredient.remove', [$recipe->id, $recipeIngredient->id]) }}" method="POST" class="btn-group-sm">
                                            @csrf
                                            @method('DELETE')
                                            <a href="{{ route('recipes.ingredient.edit', [$recipe->id, $recipeIngredient->id]) }}" class="btn btn-outline-dark">Edit</a>
                                            <button type="submit" class="btn btn-outline-danger btn-delete">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>


                <div class="card mb-4">
                    <div class="card-header">Add Steps</div>
                    <div class="card-body">
                        <form action="{{route('recipes.step.add', $recipe->id)}}" method="POST">
                            @csrf
                            @include('forms.recipesSteps')
                            <div class="text-right">
                                <button type="submit" class="btn btn-sm btn-primary">Add Steps</button>
                            </div>
                        </form>
                    </div>
                </div>


                <div class="card mb-4">
                    <div class="card-header">Recipe Steps</div>
                    <div class="card-body">
                        <table class="table table-sm">
                            <thead>
                            <tr>
                                <th scope="col" width="50">#</th>
                                <th scope="col">Desc</th>
                                <th scope="col" width="120">controls</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($recipe->recipeSteps as $step)
                                <tr>
                                    <td>{{ $step->step_number }}</td>
                                    <td>{{ $step->instructions }}</td>
                                    <td>
                                        <form action="{{ route('recipes.step.remove', [$recipe->id, $step->id]) }}" method="POST" class="btn-group-sm">
                                            @csrf
                                            @method('DELETE')
                                            <a href="{{ route('recipes.step.edit', [$recipe->id, $step->id]) }}" class="btn btn-outline-dark">Edit</a>
                                            <button type="submit" class="btn btn-outline-danger btn-delete">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
@endsection