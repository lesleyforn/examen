@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row">

            @include('incl.user-menu')

            <div class="col-sm-12 col-md-8 col-lg-10">

                @if ($message = Session::get('success'))
                    <div class="alert alert-success mb-4">
                        <p>{{ $message }}</p>
                    </div>
                @endif

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="card mb-4">
                    <div class="card-header">Edit {{isset($recipeIngredient)? 'ingredient' : 'step'}}</div>
                    <div class="card-body">

                        @if(isset($recipe) && isset($recipeIngredient))

                            <form action="{{route('recipes.ingredient.update', [$recipe->id, $recipeIngredient->id])}}" method="POST">
                                @csrf
                                @method('PUT')
                                @include('forms.recipesIngredients')
                                <div class="d-flex justify-content-between">
                                    <a href="{{ route('recipes.edit' , $recipe->id) }}" class="btn btn-sm btn-danger">Go back</a>
                                    <button type="submit" class="btn btn-sm btn-primary">Update</button>
                                </div>
                            </form>

                        @elseif(isset($recipe) && isset($step))

                            <form action="{{route('recipes.step.update', [$recipe->id, $step->id])}}" method="POST">
                                @csrf
                                @method('PUT')
                                @include('forms.recipesSteps')
                                <div class="d-flex justify-content-between">
                                    <a href="{{ route('recipes.edit' , $recipe->id) }}" class="btn btn-sm btn-danger">Go back</a>
                                    <button type="submit" class="btn btn-sm btn-primary">Update</button>
                                </div>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
@endsection