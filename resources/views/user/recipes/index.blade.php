@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">

        @include('incl.user-menu')

        <div class="col-sm-12 col-md-8 col-lg-10">
        @if(session('status') or session('login'))
            <div class="card mb-4">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                @if(session('login'))
                    <p>You are logged in!</p>
                @endif
                </div>
            </div>
        @endif

            @if ($message = Session::get('success'))
                <div class="alert alert-success mb-4">
                    <p>{{ $message }}</p>
                </div>
            @endif

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="card mb-4">
                <div class="card-header">Create Recipe</div>
                <div class="card-body">
                    <form action="{{ route('recipes.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @include('forms.recipes')

                        <div class="text-right">
                            <input type="submit" class="btn btn-sm btn-primary" value="Create">
                        </div>
                    </form>
                </div>
            </div>

            <div class="card mb-4">
                <div class="card-header">All Recipes</div>
                <div class="card-body">
                    <table class="table table-sm">
                        <thead>
                        <tr>
                            <th scope="col" width="50">#</th>
                            <th scope="col" width="100">Image</th>
                            <th scope="col">Title</th>
                            <th scope="col">Desc</th>
                            <th scope="col" width="150">Controls</th>
                        </tr>
                        </thead>
                        <tbody>

                    @foreach($recipes as $recipe)
                        <tr>
                            <th scope="row">{{ $recipe->id }}</th>
                            <td><img src="{{ Storage::disk('local')->url($recipe->image) }}" alt="..." class="img-thumbnail"/></td>
                            <td>{{ $recipe->title }}</td>
                            <td>{{ $recipe->desc }}</td>
                            <td>
                                <form action="{{ route('recipes.destroy', $recipe->id) }}" method="POST" class="btn-group-sm">
                                    @csrf
                                    @method('DELETE')
                                    <a href="{{ route('recipes.show', $recipe->id) }}" class="btn btn-outline-dark">Details</a>
                                    <button type="submit" class="btn btn-outline-danger btn-delete">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


