@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">

            @include('incl.user-menu')

            <div class="col-sm-12 col-md-8 col-lg-10">

                @if ($message = Session::get('success'))
                    <div class="alert alert-success mb-4">
                        <p>{{ $message }}</p>
                    </div>
                @endif

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="card mb-4">
                    @if(isset($ingredient))
                        <div class="card-header">Edit ingredient</div>
                        <div class="card-body">
                            <form action="{{route('ingredients.update', $ingredient->id)}}" method="POST">
                            @method('PUT')
                            @csrf

                            @include('forms.ingredients')
                                <div class="text-right">
                                    <input type="submit" class="btn btn-sm btn-primary" value="Edit">
                                </div>
                            </form>
                        </div>
                    @else
                        <div class="card-header">Create ingredient</div>
                        <div class="card-body">
                            <form action="{{ route('ingredients.store') }}" method="POST">
                            @csrf

                            @include('forms.ingredients')
                                <div class="text-right">
                                    <input type="submit" class="btn btn-sm btn-primary" value="Create">
                                </div>
                            </form>
                        </div>
                    @endif
                </div>

                <div class="card mb-4">
                    <div class="card-header">All ingredients</div>
                    <div class="card-body">
                        <table class="table table-sm">
                            <thead>
                            <tr>
                                <th scope="col" width="50">#</th>
                                <th scope="col">Name</th>
                                <th scope="col" width="120" class="text-center">Controls</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($ingredients as $single_ingredient)
                                <tr>
                                    <th scope="row">{{$single_ingredient->id  }}</th>
                                    <td>{{ ucfirst($single_ingredient->name) }}</td>
                                    <td>
                                        @if(Auth::user()->admin === 1)
                                        <form action="{{ route('ingredients.destroy', $single_ingredient->id) }}" method="POST" class="btn-group-sm text-center">
                                            <a href="{{ route('ingredients.edit', $single_ingredient->id) }}" class="btn btn-outline-dark">Edit</a>
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-outline-danger btn-delete">Delete</button>
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $ingredients->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


