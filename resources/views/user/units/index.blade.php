@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">

            @include('incl.user-menu')

            <div class="col-sm-12 col-md-8 col-lg-10">

                @if ($message = Session::get('success'))
                    <div class="alert alert-success mb-4">
                        <p>{{ $message }}</p>
                    </div>
                @endif

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                    <div class="card mb-4">
                        @if(isset($unit))
                            <div class="card-header">Edit unit</div>
                            <div class="card-body">
                                <form action="{{route('units.update', $unit->id)}}" method="POST">
                                    @method('PUT')
                                    @csrf

                                    @include('forms.units')
                                    <div class="text-right">
                                        <input type="submit" class="btn btn-sm btn-primary" value="Edit">
                                    </div>
                                </form>
                            </div>
                        @else
                            <div class="card-header">Create unit</div>
                            <div class="card-body">
                                <form action="{{ route('units.store') }}" method="POST">
                                    @csrf

                                    @include('forms.units')
                                    <div class="text-right">
                                        <input type="submit" class="btn btn-sm btn-primary" value="Create">
                                    </div>
                                </form>
                            </div>
                        @endif
                    </div>


                <div class="card mb-4">
                    <div class="card-header">All measurements units</div>
                    <div class="card-body">
                        <table class="table table-sm">
                            <thead>
                            <tr>
                                <th scope="col" width="120">#</th>
                                <th scope="col" width="120">unit</th>
                                <th scope="col">Name</th>
                                @if(Auth::user()->admin === 1)
                                    <th scope="col" width="120" class="text-center">Controls</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($units as $single_unit)
                                <tr>
                                    <th scope="row">{{$single_unit->id  }}</th>
                                    <td>{{ ucfirst($single_unit->unit) }}</td>
                                    <td>{{ ucfirst($single_unit->name) }}</td>
                                    @if(Auth::user()->admin === 1)
                                    <td>
                                        <form action="{{ route('units.destroy', $single_unit->id) }}" method="POST" class="btn-group-sm text-center">
                                            <a href="{{ route('units.edit', $single_unit->id) }}" class="btn btn-outline-dark">Edit</a>
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-outline-danger btn-delete">Delete</button>
                                        </form>
                                    </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $units->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


