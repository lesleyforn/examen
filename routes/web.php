<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PublicController@index')->name('public.landing');
Route::get('/recipes/{recipe}', 'PublicController@recipe')->name('public.recipe');
Route::post('/search', 'PublicController@search')->name('public.search');

Auth::routes();

Route::resource('/user/recipes', 'RecipesController')->except('create');
Route::resource('/user/units', 'UnitsController')->except('create','show');
Route::resource('/user/ingredients', 'IngredientsController')->except('create','show');

Route::get('/user/recipes/{recipe}/step/{step}', 'RecipesStepsController@edit' )->name('recipes.step.edit');
Route::post('/user/recipes/{recipe}/step', 'RecipesStepsController@store' )->name('recipes.step.add');
Route::put('/user/recipes/{recipe}/step/{step}', 'RecipesStepsController@update' )->name('recipes.step.update');
Route::delete('/user/recipes/{recipe}/step/{step}', 'RecipesStepsController@destroy' )->name('recipes.step.remove');

Route::get('/user/recipes/{recipe}/ingredient/{ingredient}', 'RecipesIngredientsController@edit' )->name('recipes.ingredient.edit');
Route::post('/user/recipes/{recipe}/ingredient', 'RecipesIngredientsController@store' )->name('recipes.ingredient.add');
Route::put('/user/recipes/{recipe}/ingredient/{ingredient}', 'RecipesIngredientsController@update' )->name('recipes.ingredient.update');
Route::delete('/user/recipes/{recipe}/ingredient/{ingredient}', 'RecipesIngredientsController@destroy' )->name('recipes.ingredient.remove');



